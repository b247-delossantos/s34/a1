//1. Import express using require directive
const express = require("express");

//2. Initialize express by using it to the app variable as a function
const app = express();

//3. Set the port number
const port = 4004;

//4. Use middleware to allow express to read JSON
app.use(express.json());

//5. Use middleware to allow express to be able to read more data types from a response
app.use(express.urlencoded({extended: true}));

//6. Listen to the port and console.log a text once the server is running
app.listen(port, () => console.log(`Server is running at localhost:${port}`));

//[SECTION] Routes
	//Express has methods corresponding to each HTTP method

app.get("/hello", (request, response) => {
	response.send('Hello from the /hello endpoint')
});

app.post("/display-name", (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);

});

//[SUB-SECTION] Sign-Up Form
let users = [];

app.post("/sign-up", (req, res) => {
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	} else {
		res.send("Please input BOTH username and password");
	}
});

app.put("/change-password", (req, res) =>{
	//Create a varaible to store the messahe to be sent back to the client/Postman
	let message;

	//Create a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){

		// If the username provided in the client/Postman and the username the current object on the loop is the same
		if(req.body.username == users[i].username){

			// Changes the password of the user found by the loop into the password provided in the client/Postman
			users[i].password = req.body.password;

			// Changes the message to be sent back by the response
			message = `User ${req.body.username}'s password has been updated.`;

			//Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;

		// If no user was found	
		} else {

			// Changes the message to be sent back by the response
			message = "User does note exists";
		} 
	}

	//Send a response back to the client/Postman once the password has been updated or if a user is not found.
	res.send(message);
})



//Activity


app.get("/home", (request, response) => {
	response.send('Welcome to the homepage')
});

app.get("/users", (request, response) => {
	response.send(users)
});

/*app.delete("/delete-user", (request, response) => {
	response.send(`User ${req.body.username} has been deleted.`)
});*/

app.delete("/delete-user", (req, res) => {
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} has been deleted!`);
	} else {
		res.send("Please input an item to be deleted");
	}
});
